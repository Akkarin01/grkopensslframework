Pod::Spec.new do |s|

    s.name            = "GRKOpenSSLFramework"
    s.version         = "0.0.1"
    s.summary         = "A script for compiling OpenSSL for Apple Devices"
    s.swift_version = '5'
    s.author             = { 'akkarin.phain@priorsolution.co.th' => 'akkarin.phain@priorsolution.co.th' }
    s.homepage        = "https://gitlab.com/Akkarin01/grkopensslframework"
    s.source           = { :git => 'https://gitlab.com/Akkarin01/grkopensslframework.git', :tag => s.version.to_s }
    s.license         = { :type => 'Apache', :file => 'LICENSE' }
    s.ios.deployment_target         = "9.0"
    s.osx.deployment_target         = "10.15"
    s.vendored_frameworks           = "OpenSSL-iOS/bin/openssl.framework"
    s.requires_arc                  = false
end

